﻿using System;
using System.Collections.Generic;

namespace entity_dotnet
{
    class Program
  {
    static void Main(string[] args)
    {
      var EM = EntityManager.Instance();
      var e = EM.newEntity();
      var c = new Component("test");
      c["payload"] = "456";
      var c2 = new Component("otherTest");
      c2["x"] = 20.ToString();

      EM[e].addComponent(c);
      EM[e].addComponent(c2);
      Console.WriteLine(EM.ToString());

      Console.WriteLine(EM[e][c.Key].ToString());
      Console.WriteLine(EM[e][c2.Key].ToString());
    }
  }
}
