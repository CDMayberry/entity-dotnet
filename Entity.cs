using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;

public class EntityManager {
  // Public interface
  public static EntityManager Instance() {
    return _this ??= new EntityManager();
  } 
  public string newEntity() {
    var ent = new Entity(_entities.Count.ToString());
    _entities.Add(ent.Key, ent);
    return ent.Key;
  }
  public Entity this[string key] {
    get { return _entities[key]; }
  }

   // Ctor and util
  private EntityManager() {
    _entities = new Dictionary<string, Entity>();
  }
  public string ToString(string indent="") {
    var e = "";
    foreach (var ent in _entities) {
      e += $"{System.Environment.NewLine}{ent.Value.ToString(indent+"  ")}";
    }
    return $"{indent}EntityManager [{_entities.Count} entities]:{e}";
  }

  // Private
  private Dictionary<string, Entity> _entities;
  private static EntityManager _this;
}

public class Entity {
  // Interface
  public string Key {
    get => _eid;
  }
  public void addComponent(Component component) {
    _components[component.Key] = component;
  }
  public Component this[string key] {
    get { return _components[key]; }
  }

  // Ctor and util
  public Entity(string id) {
    _eid = id;
    _components = new Dictionary<string,Component>();
  }
  public string ToString(string indent = "") {
    var c = "";
    foreach (var comp in _components) {
      c += $"{System.Environment.NewLine}{comp.Value.ToString(indent+"  ")}";
    }
    return $"{indent}Entity [{_eid}]:{c}";
  }

  // Private
  private string _eid;
  private Dictionary<string,Component> _components;

}

public class Component {
  // Interface
  public string Key {
    get { return _name; }
  }
  public string this[string key] {
    get { return _data[key]; }
    set { _data[key] = value; }
  }

  // Ctor and util
  public Component(string name) {
    _name = name;
    _data = new Dictionary<string,string>();
  }
  public string ToString(string indent = "") {
    var d = "";
    foreach (var data in _data) {
      d += $"{System.Environment.NewLine}{indent+"  "}{data.Key}: {data.Value}";
    }
    return $"{indent}Component [{_name}]:{d}";
  }

  // Private
  private Dictionary<string,string> _data;
  private string _name;
}

